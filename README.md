# fake_gui_in_pysimplegui

![App as of 08/07/2023](images/current_app_screenshot_1280_1024.png)
![App as of 08/07/2023](images/current_app_screenshot_off_call_1280_1024.png)

This project houses a very rough, fake GUI, used as a plot-device in the upcoming movie by Zach Pike.

# Dependencies
As this project is written exclusively in Python 3, and utilizes the PySimpleGUI API, the following should be the only dependencies you'll need:

*  Python 3
*  PySimpleGUI

**For GNU/Linux**

You can use your distribution's package manager to install Python 3, if it doesn't come installed by default. For example, in Debian, run the following command in your Terminal application:

    sudo apt install python3

**For Windows**

Download and install the latest Stable Release from the following link:
https://www.python.org/downloads/windows/

**For MacOS**

Download and install the latest Stable Release from the following link:
https://www.python.org/downloads/macos/

Once Python 3 is installed, barring any additional steps regarding updating your PATH variable, you can run the following command to quickly and easily install PySimpleGUI:

    python3 -m pip install pysimplegui

Once the dependencies are installed and the application (app.py) is downloaded, running the application should be as simple as running the following command in your OS's command-line environment, provided you're in the same directory where you downloaded app.py:

    python3 ./app.py
